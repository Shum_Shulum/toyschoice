// Copyright Epic Games, Inc. All Rights Reserved.

#include "ToysChoice.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ToysChoice, "ToysChoice" );

DEFINE_LOG_CATEGORY(LogToysChoice)
 