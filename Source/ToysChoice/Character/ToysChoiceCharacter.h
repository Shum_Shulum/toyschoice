// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ToysChoice/FuncLibrary/Types.h"
#include "ToysChoiceCharacter.generated.h"

UCLASS(Blueprintable)
class AToysChoiceCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AToysChoiceCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	// Overriding Input for the character
	virtual void SetupPlayerInputComponent(UInputComponent* NewInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

public:

//Functions
	
	/** Function will be worked when player press W or S **/
	UFUNCTION()
	void InputAxisX (float Value);

	/** Function will be worked when player press A or D **/
	UFUNCTION()
	void InputAxisY (float Value);

	/** Variables for InputAxisX and InputAxisY **/
	float AxisX = 0.0f;
	float AxisY = 0.0f;

	/** Tick function **/
	UFUNCTION()
	void MovementTick (float DeltaTime);

	/** This function has basic movement states. It will be used in ChangeMovementState function **/
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();

	/** This function created new movement state for player's character **/
	UFUNCTION(BlueprintCallable)
	void ChangeMovementState ();

	/** Wasting stamina while sprinting **/
	UFUNCTION(BlueprintCallable)
	void ActionWithStamina (float DeltaTime);
	
//Variables
	
	/** Default MovementState **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	EMovementState MovementState = EMovementState::Run_State;

	/** Variable by Types struct **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	FCharacterSpeed MovementSpeedInfo;

	/** Variable setting movement state to terms **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	bool SprintRunEnabled = false;

	/** Variable setting movement state to terms **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	bool WalkEnabled = false;

	/** Variable setting movement state to terms **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	bool AimEnabled = false;

	/** This variable used for sprint run **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	float Stamina = 10.0f;

};

