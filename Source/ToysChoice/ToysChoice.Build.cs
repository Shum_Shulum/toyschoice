// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ToysChoice : ModuleRules
{
	public ToysChoice(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
