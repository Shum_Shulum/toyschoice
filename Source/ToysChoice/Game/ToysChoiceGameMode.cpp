// Copyright Epic Games, Inc. All Rights Reserved.

#include "ToysChoiceGameMode.h"
#include "ToysChoicePlayerController.h"
#include "ToysChoice/Character/ToysChoiceCharacter.h"
#include "UObject/ConstructorHelpers.h"

AToysChoiceGameMode::AToysChoiceGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AToysChoicePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
} 