// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ToysChoiceGameMode.generated.h"

UCLASS(minimalapi)
class AToysChoiceGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AToysChoiceGameMode();
};



